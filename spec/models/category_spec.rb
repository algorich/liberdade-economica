# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Category do

    it { should have_valid(:position).when('some random stuff') }

    it { should_not have_valid(:name).when('', nil) }
    it { should have_valid(:name).when('my name') }

    it { should have_valid(:nodes).when([build(:node)]) }
end
