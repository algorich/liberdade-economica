# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Edition do
    it { should_not have_valid(:name).when('', nil) }
    it { should have_valid(:name).when('some edition') }
    it { should have_valid(:nodes).when([build(:node)]) }
    it { should have_valid(:proposals).when([build(:proposal)]) }

    context "node creation through fake_node attribute" do

      before :all do
        @parent_category = create :category
        @child_one = create :category
        @child_two = create :category
        @grand_child = create :category
        node_fake = ActiveSupport::JSON.encode [["1.", @parent_category.id], ["1.1.", @child_one.id], ["1.1.1.", @grand_child.id], ["1.2.", @child_two.id]]
        @e = create :edition, node_fake: node_fake
      end

      it "should be able to create corresponding nodes with their categories" do
        @e.errors.should be_empty
        @e.nodes.size.should == 4
        node = @e.nodes.first
        node.category.should == @parent_category
        node.children.each { |child| child.ancestry.should == node.id.to_s }
        node.children.first.category.should == @child_one
        node.children.second.category.should == @child_two         
        grand_child = node.children.first.children.first
        grand_child.category.should == @grand_child
      end

      it "should not recreate the entire tree when the model is edited" do
        new_node_fake = node_fake = ActiveSupport::JSON.encode [["1.", @parent_category.id], ["1.1.", @child_two.id], ["1.1.1.", @grand_child.id], ["1.2.", @child_one.id]]
        @e.node_fake = new_node_fake
        @e.save
        @e.nodes.reload
        @e.nodes.size.should == 4        
        node = @e.nodes.first
        node.category.should == @parent_category
        node.children.each { |child| child.ancestry.should == node.id.to_s }
        node.children.first.category.should == @child_two
        node.children.second.category.should == @child_one        
        grand_child = node.children.first.children.first
        grand_child.category.should == @grand_child
      end

      context "category validation" do
        it "should not create the nodes when one category doesn't exist" do
          parent_category = create :category
          child_two = create :category
          non_existing_category_id = 99999
          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.", non_existing_category_id], ["1.2.", child_two.id]]
          e = create :edition, node_fake: node_fake

          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid category.']
        end
      end

      context "hierarchy validation" do

        it "should not create the nodes when the hierarchy isn't valid" do
          parent_category = create :category
          child_one = create :category
          child_two = create :category

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.", child_one.id], ["1.3.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.", child_one.id], ["3.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.3.", child_one.id], ["2.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.", child_one.id], ["2.1.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1.1.", child_one.id], ["2.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.1.", parent_category.id], ["1.1.1.", child_one.id], ["2.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["2.", parent_category.id], ["2.1.1.", child_one.id], ["2.1.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1.", parent_category.id], ["1.1", child_one.id], ["2.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']

          node_fake = ActiveSupport::JSON.encode [["1", parent_category.id], ["1.1.", child_one.id], ["2.", child_two.id]]
          e = create :edition, node_fake: node_fake
          e.errors.should_not be_empty
          e.errors[:node_fake].should == ['Invalid hierarchy.']
        end
      end
    end

end
