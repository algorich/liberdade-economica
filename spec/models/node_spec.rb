# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Node do
    it { should have_valid(:category).when(build :category) }
    it { should have_valid(:edition).when(build :edition) }

    it { should have_valid(:parent).when(create :node, nil) }

    it 'should have valid children' do
      node = create :node
      child = node.children.create! name: 'child node'
      node.children.should == [child]
    end
end
