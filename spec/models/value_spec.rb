# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Value do
  it { should have_valid(:edition).when(build(:edition)) }
  it { should have_valid(:country).when(build(:country)) }
end
