#encoding: utf-8

require 'spec_helper'

feature 'Country Management' do

  background do
    create :configuration
    user = create :user
    login(user.email, user.password)
    @country = create :country
  end

  context 'creation' do
    before :each do
      visit '/admin/country/new'
    end

    scenario 'successful creation' do
      fill_in 'Nome', with: 'Brasil'
      fill_in 'Código', with: 'BR'

      first(:button, 'Salvar').click
      page.should have_content('País criado(a) com sucesso.')
    end
  end

  context 'edit' do
    before :each do
      visit "/admin/country/#{@country.id}/edit"
    end

    scenario 'successfully' do
      fill_in 'Nome', with: 'Brasil'
      fill_in 'Código', with: 'BR'
      first(:button, 'Salvar').click
      page.should have_content 'País atualizado(a) com sucesso.'
    end

    context 'unsuccessfully' do
      scenario 'empty name' do
        fill_in 'Nome', with: ''
        first(:button, 'Salvar').click
        page.should have_content('Nome não pode ser vazio.')
      end

      scenario 'empty code' do
        fill_in 'Código', with: ''
        first(:button, 'Salvar').click
        page.should have_content('Código não pode ser vazio.')
      end
    end
  end

end
