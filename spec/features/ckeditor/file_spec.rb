# -*- encoding : utf-8 -*-

require 'spec_helper'

feature 'upload files' do
  background do
    @user = FactoryGirl.create :user, :email => 'user@user.com', :password => '123456'
    @configuration = FactoryGirl.create :configuration
    login(@user.email,'123456')
  end

  context 'add a file' do
    it 'successfully' do
      first(:link, 'Arquivos').click
      first(:link, 'Adicionar').click
      attach_file 'Arquivo', "#{Rails.root}/spec/data/file.pdf"
      first(:button, 'Salvar').click
      page.should have_content 'Arquivo criado(a) com sucesso.'
    end

    it 'with errors' do
      first(:link, 'Arquivos').click
      first(:link, 'Adicionar').click
      first(:button, 'Salvar').click
      page.should have_content 'Arquivo criado(a) com falha.'
    end
  end

  it 'remove a file' do
    file = FactoryGirl.create :ckeditor_file
    first(:link, 'Arquivos').click
    find("a[@href='/admin/ckeditor~attachment_file/#{file.id}/delete']").click
    click_button 'Sim, eu tenho certeza.'
    page.should have_content 'Arquivo excluído(a) com sucesso.'
  end
end
