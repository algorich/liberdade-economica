#encoding: utf-8

require 'spec_helper'

feature 'Edition Management' do

  background do
    create :configuration
    user = create :user
    login(user.email, user.password)
    @edition = create :edition
  end

  context 'creation' do
    before :each do
      visit '/admin/edition/new'
    end

    scenario 'successful creation' do
      fill_in 'Nome', with: 'Primeira'
      fill_in 'Data de divulgação', with: I18n.t(Time.now, format: :datepicker)

      first(:button, 'Salvar').click
      page.should have_content('Edição criado(a) com sucesso.')
    end
  end

  context 'edit' do
    before :each do
      visit "/admin/edition/#{@edition.id}/edit"
    end

    scenario 'successfully' do
      fill_in 'Nome', with: 'Primeira'
      fill_in 'Data de divulgação', with: I18n.t(Time.now, format: :datepicker)

      first(:button, 'Salvar').click
      page.should have_content 'Edição atualizado(a) com sucesso.'
    end

    context 'unsuccessfully' do
      scenario 'empty name' do
        fill_in 'Nome', with: ''
        first(:button, 'Salvar').click
        page.should have_content('Nome não pode ser vazio.')
      end

      scenario 'empty date' do
        fill_in 'Data de divulgação', with: ''
        first(:button, 'Salvar').click
        page.should have_content('Data de divulgação não pode ser vazio.')
      end
    end
  end

end
