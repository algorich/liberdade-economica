FactoryGirl.define do
  factory :node do
    edition { FactoryGirl.create :edition }
    category { FactoryGirl.create :category }
  end
end
