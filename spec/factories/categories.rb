# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :category do

    sequence :name do |n|
     "Category #{n}"
    end

    sequence :position do |n|
     "Position #{n}"
    end
  end
end
