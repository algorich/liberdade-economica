class ValuesLabelsSum < ActiveRecord::Base
  attr_accessible :label, :edition, :sum, :category_id, :edition_id

  belongs_to :label
  belongs_to :edition
end
