# -*- encoding : utf-8 -*-
class Page < ActiveRecord::Base

  attr_accessible :title, :content, :published

  validates_presence_of :title

  PAGES = {
    :contact => 1
  }

  rails_admin do
    list do
      field :title
      field :published
    end

    edit do
      field :title
      field(:content) { ckeditor true }
      field :published
    end
  end
end
