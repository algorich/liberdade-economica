# -*- encoding : utf-8 -*-

class Ckeditor::AttachmentFile < Ckeditor::Asset
  has_attached_file :data,
    :url => '/system/ckeditor_assets/attachments/:id/:filename',
    :path => ':rails_root/public/system/ckeditor_assets/attachments/:id/:filename'

  validates_attachment_size :data, :less_than => 100.megabytes
  validates_attachment_presence :data

  attr_accessible :data

  def url_thumb
    @url_thumb ||= Ckeditor::Utils.filethumb(filename)
  end

  rails_admin do
    label 'Arquivo'
    navigation_label 'Arquivos adicionados através do editor'
    weight 1

    edit do
      field(:data) { label 'Arquivo' }
    end

    list do
      field(:data) do
        label 'Arquivo'
         pretty_value do
          # used in list view columns and show views, defaults to
          # formatted_value for non-association fields
          "<a href=\"#{value.url}\" target=\"_blank\">#{value.original_filename}</a>".html_safe
        end
      end

      field(:created_at) { label 'Criado em' }
      field(:updated_at) { label 'Atualizado em' }
    end
  end
end
