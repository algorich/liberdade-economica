class Proponent < ActiveRecord::Base
  has_many :proposals, dependent: :destroy
  # attr_accessible :title, :body
end
