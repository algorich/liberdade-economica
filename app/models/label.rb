class Label < ActiveRecord::Base
  attr_accessible :name, :countries, :editions

  has_and_belongs_to_many :countries

  has_many :values_labels_sum
  has_many :editions, through: :values_labels_sum

  validates :name, presence: true
end
