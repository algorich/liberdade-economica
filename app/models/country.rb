class Country < ActiveRecord::Base
  attr_accessible :name, :labels, :label_ids, :code, :description

  has_many :values, dependent: :destroy
  has_and_belongs_to_many :labels

  validates :name, presence: true
  validates :code, presence: true

  def situation edition
    total = Value.where(edition_id: edition.id, country_id: self.id).first.total

    if    total <= 10.0 && total >= 8.0
      return 'free'
    elsif total  < 8.0  && total >= 7.0
      return 'mostly-free'
    elsif total  < 7.0  && total >= 6.0
      return 'moderately-free'
    elsif total  < 6.0  && total >= 5.0
      return 'mostly-unfree'
    end

    return 'unfree'
  end
end
