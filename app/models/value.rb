class Value < ActiveRecord::Base
  attr_accessible :edition, :edition_id, :country, :country_id, :total, :values

  belongs_to :edition
  belongs_to :country

  validates_presence_of :edition_id, :country_id

  def create_values_sum
    unless self.values.blank?
      vals = ActiveSupport::JSON.decode(self.values)

      vals.each do |v|
        value_sum = ValuesSum.where(edition_id: self.edition_id,
          category_id: v[0]).first_or_create

        (value_sum.sum += v[1].to_f) unless v[1].nil?
        value_sum.save!
      end
    end

    unless self.total.nil?
      self.country.labels.each do |l|
        value_label_sum = ValuesLabelsSum.where(edition_id: self.edition_id,
          label_id: l.id).first_or_create

        value_label_sum.sum += self.total
        value_label_sum.save!
      end

      self.edition.total_sum = 0 if self.edition.total_sum.nil?
      self.edition.total_sum += self.total
      self.edition.save!
    end
  end

  class << self
    def update_aux_values
      conf = Configuration.first
      conf.update_attribute(maintenance: true)

      Edition.update_all(total_sum: 0)
      ValuesLabelsSum.update_all(sum: 0)
      ValuesSum.update_all(sum: 0)

      Value.all.each { |v| v.create_values_sum }

      conf.update_attribute(maintenance: false)
    end
    handle_asynchronously :update_aux_values
  end

  def category id
    unless self.values.blank?
      values = ActiveSupport::JSON.decode(self.values)
      value = (values.select { |v| v[0].to_s == id }).first

      return value[1].gsub(',', '.') unless value.blank? || value[1].blank?
    end

    return 'null'
  end
end