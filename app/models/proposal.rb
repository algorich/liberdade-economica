class Proposal < ActiveRecord::Base
  attr_accessible :content, :proponent

  belongs_to :proponent
  belongs_to :edition

  validates :content, presence: true
end
