class Node < ActiveRecord::Base
  attr_accessible :edition, :category, :ancestry
  has_ancestry

  belongs_to :edition
  belongs_to :category
end
