//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.select2.min
//= require jquery-jvectormap-1.2.2.min
//= require jquery-jvectormap-world-mill-en
//= require ftscroller
//= require bootstrap

$(function () {
    var elem = document.getElementById('app-index-categories-tree');

    if (elem != null) new FTScroller(elem, { scrollingX: false });
});

$(function () {
    $('#app-chzn-select-country').select2().change(function (val) {
        console.log(val.val);
        $('#app-country-link').attr('href', val.val);
    });
});

function defineUrl () {
    var query_string = '?';

    $.each($('#data-page #app-index-labels li.active'), function (i, val) {
        query_string += '&label[]=' + $(val).data('id');
    });

    $('#data-page #link_graph_aux').attr('href',
        $('#app-index-categories-tree .active > a').attr('href') +
            query_string);
}

$(function () {
    $('#data-page #app-index-labels li').click(function () {
        $(this).toggleClass('active');
        defineUrl();

        $('#data-page #link_graph_aux').click();
    });
});

$(function () {
    $items = $('#data-page #app-index-categories-tree li');

    $items.find('a').click(function () {
        $items.removeClass('active');
        $(this).parent().addClass('active');
        defineUrl();

        $('#data-page #link_graph_aux').click();

        return false;
    });
});

$(function () {
    $('.app-tooltip').hover(function (e) {
        $(this).tooltip('toggle');

        e.preventDefault();
    })
});
