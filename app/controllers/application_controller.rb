# -*- encoding : utf-8 -*-

class ApplicationController < ActionController::Base

  protect_from_forgery

  layout :layout_by_resource
  before_filter :check_maintenance


  def layout_by_resource
    if devise_controller?
      'login'
    else
      'application'
    end
  end

  def check_maintenance

    unless params[:controller].include? 'rails_admin'
      render 'site/maintenance', layout: false if ::Configuration.first.maintenance
    end
  end
end
