# -*- encoding : utf-8 -*-

class SiteController < ApplicationController

  before_filter Proc.new {
    @editions = Edition.where(published: true).order('date ASC')

    @last_edition = @editions.last

    @penultimate_edition = @editions.order('date ASC').last(2).first

    unless @penultimate_edition.nil?
      @penultimate_edition_values = @penultimate_edition.values
    end

    @countries_ranking = Country.includes(:values).
      where('values.edition_id = ?', @last_edition.id).
        order('values.total DESC')

  }, except: [:graph, :general_graph, :change_map, :update_vals, :send_contact]

  def index
    @values = @last_edition.values.order('total DESC')
    @penultimate_countries_ranking = Country.includes(:values).
      where('values.edition_id = ?', @penultimate_edition.id).
        order('values.total DESC')
  end

  def ranking
    @editions.each do |e|
      @edition = e if e.name.parameterize('-') == params[:edition]
    end

    unless @edition.nil?
      @values = @edition.values.order('total DESC')

      if @editions.first != @edition
        @penultimate_countries_ranking = Country.includes(:values).
          where('values.edition_id = ?', @editions.index(@edition) - 1).
            order('values.total DESC')
      end
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def country
    @countries_ranking.each do |c|
      @country = c if c.name.parameterize('-') == params[:name]
    end

    unless @country.nil?
      @editions = Edition.includes(:values).order('date ASC')
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def comparate
    @countries_ranking.each do |c|
      @country1 = c if c.name.parameterize('-') == params[:name1]
    end

    @countries_ranking.each do |c|
      @country2 = c if c.name.parameterize('-') == params[:name2]
    end

    if @country1.nil? or @country2.nil?
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def page
    Page.where(published: true).each do |p|
      @page = p if p.title.parameterize == params[:page_title]
    end

    raise ActionController::RoutingError.new('Not Found') if @page.nil?

    @contact_form = Contact.new if ::Configuration.last.contact_page_id == @page.id
  end

  def graph
    @category = Category.find(params[:category])
    @country1 = Country.find(params[:country_1]) unless params[:country_1].nil?
    @country2 = Country.find(params[:country_2]) unless params[:country_2].nil?

    @editions = Edition.where(published: true).order('date ASC').
      includes(:values)

    respond_to do |format|
      format.js
    end
  end

  def general_graph
    @category = Category.find(params[:category])
    @editions = Edition.where(published: true).order('date ASC').
      includes(:values)

    respond_to do |format|
      format.js
    end
  end


  def change_map
    @edition = Edition.find(params[:id])
    @values = @edition.values.order('total DESC')

    respond_to do |format|
      format.js
    end
  end

  def update_vals
    Value.update_aux_values

    respond_to do |format|
      format.js
    end
  end

  # CONTACT BEGIN

  def send_contact
    @page = ::Configuration.last.contact_page
    @contact_form = Contact.new(params[:contact])

    if @contact_form.save
      redirect_to(page_path(@page.title.parameterize),
        notice: 'Mensagem enviada com sucesso.')
    else
      render action: :page
    end
  end

  # CONTACT END
end
