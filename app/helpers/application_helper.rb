# -*- encoding : utf-8 -*-
module ApplicationHelper
  def configuration
    @configuration ||= Configuration.first
  end

  def generate_categories_list nodes, type, country_1_id=nil, country_2_id=nil, active=nil
    if nodes.empty?
      return ''
    else
      html_return = ''

      nodes.each do |n|
        category = n.keys.first[1]
        elem_class = active == category ? ' class="active"' : ''
        route = type == 'normal' ? graph_path(category.id, country_1_id,
          country_2_id) : general_graph_path(category.id)

        html_return << "<li#{elem_class} data-id=\"#{category.id}\">
            #{link_to ('<span>' + category.name + '</span>').html_safe, route,
              remote: true, title: category.name}
            #{generate_categories_list n[n.keys.first], type, country_1_id, country_2_id}
          </li>"
      end

      return "<ul>#{html_return}</ul>".html_safe
    end
  end

end
