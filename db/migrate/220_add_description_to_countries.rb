class AddDescriptionToCountries < ActiveRecord::Migration
  def up
    add_column :countries, :description, :text
  end

  def down
    remove_column :countries, :description
  end
end
