class CreateValuesLabelsSums < ActiveRecord::Migration
  def change
    create_table :values_labels_sums do |t|
      t.references :label
      t.references :edition
      t.float :sum, default: 0

      t.timestamps
    end

#    Value.all.each do |val|
#      unless val.values.blank?
#        vals = ActiveSupport::JSON.decode(val.values)

#        vals.each do |v|
#          value_sum = ValuesSum.where(edition_id: val.edition_id,
#            category_id: v[0]).first_or_create

#          (value_sum.sum += v[1].to_f) unless v[1].nil?
#          value_sum.save!
#        end
#      end

#      unless val.total.nil?
#        val.country.labels.each do |l|
#          value_label_sum = ValuesLabelsSum.where(edition_id: val.edition_id,
#            label_id: l.id).first_or_create

#          value_label_sum.sum += val.total
#          value_label_sum.save!
#        end

#        val.edition.total_sum = 0 if val.edition.total_sum.nil?
#        val.edition.total_sum += val.total
#        val.edition.save!
#      end
#    end

  end
end
