class AddFreedomValToConfigurations < ActiveRecord::Migration
  def up
    add_column :configurations, :freedom_val, :float
  end

  def down
    remove_column :configurations, :freedom_val
  end
end
