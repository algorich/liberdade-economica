class AddHiddenFieldToConfigurations < ActiveRecord::Migration
  def change
    add_column :configurations, :hidden_field, :integer
  end
end
