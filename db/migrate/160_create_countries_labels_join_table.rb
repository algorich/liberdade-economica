# -*- encoding : utf-8 -*-

class CreateCountriesLabelsJoinTable < ActiveRecord::Migration
  def change
    create_table :countries_labels do |t|
      t.references :country
      t.references :label
    end
  end
end
