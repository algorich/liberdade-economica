class AddTotalSumToEditions < ActiveRecord::Migration
  def up
    add_column :editions, :total_sum, :float
  end

  def down
    remove_column :editions, :total_sum
  end
end
