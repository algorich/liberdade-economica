class AddSocialToConfigurations < ActiveRecord::Migration
  def up
    add_column :configurations, :twitter, :string
    add_column :configurations, :facebook, :string
    add_column :configurations, :facebook_app_id, :string
  end

  def down
    remove_column :configurations, :twitter
    remove_column :configurations, :facebook
    remove_column :configurations, :facebook_app_id
  end
end
