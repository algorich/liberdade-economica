class AddContactFormToPages < ActiveRecord::Migration
  def change
    add_column :pages, :contact_form, :boolean, :default => false
  end
end
