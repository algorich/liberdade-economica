class AddMaintenanceToConfiguration < ActiveRecord::Migration
  def change
    add_column :configurations, :maintenance, :boolean, default: false
  end
end
