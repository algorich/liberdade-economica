# -*- encoding : utf-8 -*-

ValuesSum.delete_all
ValuesLabelsSum.delete_all

User.delete_all

User.create!(
  :email => 'admin@base.com',
  :password => '123456'
)

Page.delete_all

Page.create!(
  :title => 'Página 1',
  :content => 'Conteúdo',
  :published => true
)

Page.create!(
  :title => 'Página 2',
  :content => 'Conteúdo',
  :published => true
)

Page.create!(
  :title => 'Página 3',
  :content => 'Conteúdo',
  :published => true
)

contact_page = Page.create!(
  :title => 'Contato',
  :content => 'Conteúdo',
  :published => true
)

Configuration.delete_all

Configuration.create!(
  :email => 'base@project.com.br',
  :contact_page_id => contact_page.id
)

# Labels

Label.delete_all

labels = [ 'América', 'América Latina', 'América do Norte', 'Europa', 'OCDE' ]

labels.each do |l|
  Label.create!(name: l)
end

# Countries

Country.delete_all

countries = {
  'AF' => 'Afghanistan',
  'AL' => 'Albania',
  'DZ' => 'Algeria',
  'AS' => 'American Samoa',
  'AD' => 'Andorra',
  'AO' => 'Angola',
  'AI' => 'Anguilla',
  'AQ' => 'Antarctica',
  'AG' => 'Antigua and Barbuda',
  'AR' => 'Argentina',
  'AM' => 'Armenia',
  'AW' => 'Aruba',
  'AU' => 'Australia',
  'AT' => 'Austria',
  'AZ' => 'Azerbaijan',
#  'BS' => 'Bahamas',
#  'BH' => 'Bahrain',
#  'BD' => 'Bangladesh',
#  'BB' => 'Barbados',
#  'BY' => 'Belarus',
#  'BE' => 'Belgium',
#  'BZ' => 'Belize',
#  'BJ' => 'Benin',
#  'BM' => 'Bermuda',
#  'BT' => 'Bhutan',
#  'BO' => 'Bolivia',
#  'BA' => 'Bosnia and Herzegovina',
#  'BW' => 'Botswana',
#  'BV' => 'Bouvet Island',
  'BR' => 'Brazil'
#  'BQ' => 'British Antarctic Territory',
#  'IO' => 'British Indian Ocean Territory',
#  'VG' => 'British Virgin Islands',
#  'BN' => 'Brunei',
#  'BG' => 'Bulgaria',
#  'BF' => 'Burkina Faso',
#  'BI' => 'Burundi',
#  'KH' => 'Cambodia',
#  'CM' => 'Cameroon',
#  'CA' => 'Canada',
#  'CT' => 'Canton and Enderbury Islands',
#  'CV' => 'Cape Verde',
#  'KY' => 'Cayman Islands',
#  'CF' => 'Central African Republic',
#  'TD' => 'Chad',
#  'CL' => 'Chile',
#  'CN' => 'China',
#  'CX' => 'Christmas Island',
#  'CC' => 'Cocos [Keeling] Islands',
#  'CO' => 'Colombia',
#  'KM' => 'Comoros',
#  'CG' => 'Congo - Brazzaville',
#  'CD' => 'Congo - Kinshasa',
#  'CK' => 'Cook Islands',
#  'CR' => 'Costa Rica',
#  'HR' => 'Croatia',
#  'CU' => 'Cuba',
#  'CY' => 'Cyprus',
#  'CZ' => 'Czech Republic',
#  'CI' => 'Côte d’Ivoire',
#  'DK' => 'Denmark',
#  'DJ' => 'Djibouti',
#  'DM' => 'Dominica',
#  'DO' => 'Dominican Republic',
#  'NQ' => 'Dronning Maud Land',
#  'DD' => 'East Germany',
#  'EC' => 'Ecuador',
#  'EG' => 'Egypt',
#  'SV' => 'El Salvador',
#  'GQ' => 'Equatorial Guinea',
#  'ER' => 'Eritrea',
#  'EE' => 'Estonia',
#  'ET' => 'Ethiopia',
#  'FK' => 'Falkland Islands',
#  'FO' => 'Faroe Islands',
#  'FJ' => 'Fiji',
#  'FI' => 'Finland',
#  'FR' => 'France',
#  'GF' => 'French Guiana',
#  'PF' => 'French Polynesia',
#  'TF' => 'French Southern Territories',
#  'FQ' => 'French Southern and Antarctic Territories',
#  'GA' => 'Gabon',
#  'GM' => 'Gambia',
#  'GE' => 'Georgia',
#  'DE' => 'Germany',
#  'GH' => 'Ghana',
#  'GI' => 'Gibraltar',
#  'GR' => 'Greece',
#  'GL' => 'Greenland',
#  'GD' => 'Grenada',
#  'GP' => 'Guadeloupe',
#  'GU' => 'Guam',
#  'GT' => 'Guatemala',
#  'GG' => 'Guernsey',
#  'GN' => 'Guinea',
#  'GW' => 'Guinea-Bissau',
#  'GY' => 'Guyana',
#  'HT' => 'Haiti',
#  'HM' => 'Heard Island and McDonald Islands',
#  'HN' => 'Honduras',
#  'HK' => 'Hong Kong SAR China',
#  'HU' => 'Hungary',
#  'IS' => 'Iceland',
#  'IN' => 'India',
#  'ID' => 'Indonesia',
#  'IR' => 'Iran',
#  'IQ' => 'Iraq',
#  'IE' => 'Ireland',
#  'IM' => 'Isle of Man',
#  'IL' => 'Israel',
#  'IT' => 'Italy',
#  'JM' => 'Jamaica',
#  'JP' => 'Japan',
#  'JE' => 'Jersey',
#  'JT' => 'Johnston Island',
#  'JO' => 'Jordan',
#  'KZ' => 'Kazakhstan',
#  'KE' => 'Kenya',
#  'KI' => 'Kiribati',
#  'KW' => 'Kuwait',
#  'KG' => 'Kyrgyzstan',
#  'LA' => 'Laos',
#  'LV' => 'Latvia',
#  'LB' => 'Lebanon',
#  'LS' => 'Lesotho',
#  'LR' => 'Liberia',
#  'LY' => 'Libya',
#  'LI' => 'Liechtenstein',
#  'LT' => 'Lithuania',
#  'LU' => 'Luxembourg',
#  'MO' => 'Macau SAR China',
#  'MK' => 'Macedonia',
#  'MG' => 'Madagascar',
#  'MW' => 'Malawi',
#  'MY' => 'Malaysia',
#  'MV' => 'Maldives',
#  'ML' => 'Mali',
#  'MT' => 'Malta',
#  'MH' => 'Marshall Islands',
#  'MQ' => 'Martinique',
#  'MR' => 'Mauritania',
#  'MU' => 'Mauritius',
#  'YT' => 'Mayotte',
#  'FX' => 'Metropolitan France',
#  'MX' => 'Mexico',
#  'FM' => 'Micronesia',
#  'MI' => 'Midway Islands',
#  'MD' => 'Moldova',
#  'MC' => 'Monaco',
#  'MN' => 'Mongolia',
#  'ME' => 'Montenegro',
#  'MS' => 'Montserrat',
#  'MA' => 'Morocco',
#  'MZ' => 'Mozambique',
#  'MM' => 'Myanmar [Burma]',
#  'NA' => 'Namibia',
#  'NR' => 'Nauru',
#  'NP' => 'Nepal',
#  'NL' => 'Netherlands',
#  'AN' => 'Netherlands Antilles',
#  'NT' => 'Neutral Zone',
#  'NC' => 'New Caledonia',
#  'NZ' => 'New Zealand',
#  'NI' => 'Nicaragua',
#  'NE' => 'Niger',
#  'NG' => 'Nigeria',
#  'NU' => 'Niue',
#  'NF' => 'Norfolk Island',
#  'KP' => 'North Korea',
#  'VD' => 'North Vietnam',
#  'MP' => 'Northern Mariana Islands',
#  'NO' => 'Norway',
#  'OM' => 'Oman',
#  'PC' => 'Pacific Islands Trust Territory',
#  'PK' => 'Pakistan',
#  'PW' => 'Palau',
#  'PS' => 'Palestinian Territories',
#  'PA' => 'Panama',
#  'PZ' => 'Panama Canal Zone',
#  'PG' => 'Papua New Guinea',
#  'PY' => 'Paraguay',
#  'YD' => 'People\'s Democratic Republic of Yemen',
#  'PE' => 'Peru',
#  'PH' => 'Philippines',
#  'PN' => 'Pitcairn Islands',
#  'PL' => 'Poland',
#  'PT' => 'Portugal',
#  'PR' => 'Puerto Rico',
#  'QA' => 'Qatar',
#  'RO' => 'Romania',
#  'RU' => 'Russia',
#  'RW' => 'Rwanda',
#  'RE' => 'Réunion',
#  'BL' => 'Saint Barthélemy',
#  'SH' => 'Saint Helena',
#  'KN' => 'Saint Kitts and Nevis',
#  'LC' => 'Saint Lucia',
#  'MF' => 'Saint Martin',
#  'PM' => 'Saint Pierre and Miquelon',
#  'VC' => 'Saint Vincent and the Grenadines',
#  'WS' => 'Samoa',
#  'SM' => 'San Marino',
#  'SA' => 'Saudi Arabia',
#  'SN' => 'Senegal',
#  'RS' => 'Serbia',
#  'CS' => 'Serbia and Montenegro',
#  'SC' => 'Seychelles',
#  'SL' => 'Sierra Leone',
#  'SG' => 'Singapore',
#  'SK' => 'Slovakia',
#  'SI' => 'Slovenia',
#  'SB' => 'Solomon Islands',
#  'SO' => 'Somalia',
#  'ZA' => 'South Africa',
#  'GS' => 'South Georgia and the South Sandwich Islands',
#  'KR' => 'South Korea',
#  'ES' => 'Spain',
#  'LK' => 'Sri Lanka',
#  'SD' => 'Sudan',
#  'SR' => 'Suriname',
#  'SJ' => 'Svalbard and Jan Mayen',
#  'SZ' => 'Swaziland',
#  'SE' => 'Sweden',
#  'CH' => 'Switzerland',
#  'SY' => 'Syria',
#  'ST' => 'São Tomé and Príncipe',
#  'TW' => 'Taiwan',
#  'TJ' => 'Tajikistan',
#  'TZ' => 'Tanzania',
#  'TH' => 'Thailand',
#  'TL' => 'Timor-Leste',
#  'TG' => 'Togo',
#  'TK' => 'Tokelau',
#  'TO' => 'Tonga',
#  'TT' => 'Trinidad and Tobago',
#  'TN' => 'Tunisia',
#  'TR' => 'Turkey',
#  'TM' => 'Turkmenistan',
#  'TC' => 'Turks and Caicos Islands',
#  'TV' => 'Tuvalu',
#  'UM' => 'U.S. Minor Outlying Islands',
#  'PU' => 'U.S. Miscellaneous Pacific Islands',
#  'VI' => 'U.S. Virgin Islands',
#  'UG' => 'Uganda',
#  'UA' => 'Ukraine',
#  'SU' => 'Union of Soviet Socialist Republics',
#  'AE' => 'United Arab Emirates',
#  'GB' => 'United Kingdom',
#  'US' => 'United States',
#  'ZZ' => 'Unknown or Invalid Region',
#  'UY' => 'Uruguay',
#  'UZ' => 'Uzbekistan',
#  'VU' => 'Vanuatu',
#  'VA' => 'Vatican City',
#  'VE' => 'Venezuela',
#  'VN' => 'Vietnam',
#  'WK' => 'Wake Island',
#  'WF' => 'Wallis and Futuna',
#  'EH' => 'Western Sahara',
#  'YE' => 'Yemen',
#  'ZM' => 'Zambia',
#  'ZW' => 'Zimbabwe',
#  'AX' => 'Åland Islands'
}

countries.keys.each do |k|
  Country.create!(name: countries[k], code: k,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin orci at elit suscipit, vel elementum turpis fermentum. In at ante eget mi vulputate ullamcorper vel ac ante. Cras sit amet aliquam enim, ac vestibulum nibh. Suspendisse accumsan dapibus fermentum. Cras gravida et mi ac aliquet. Pellentesque euismod magna at sollicitudin tincidunt. Mauris felis lacus, faucibus ac purus facilisis, mollis elementum ligula.

    Pellentesque eget velit non metus vulputate condimentum a sit amet turpis. Nullam egestas tempus consequat. Etiam tincidunt diam non bibendum volutpat.',
    labels: Label.all.collect { |l| l if rand(2) == 1  }.compact)
end

# Indicators

Category.delete_all

categories = [
  'Liberdade Econômica', 'Regulação',
  'Liberdade para comercializar internacionalmente',
  'Dinheiro Confiável', 'Sistema Legal e Direitos de Propriedade',
  'Tamanho do Governo', 'Liberdade Pessoal', 'Relacionamento',
  'Expressão', 'Movimento', 'Segurança'
]

categories_elem = []

categories.each do |c|
  categories_elem << Category.create!(name: c)
end

# Editions

Edition.delete_all

20.times do |i|
  Edition.create!(name: "20#{i + 10}", date: "Mon, 08 Jul 20#{i + 10}",
    published: true,
    node_fake: "[[\"1.\",   #{categories_elem[ 0].id}],
                 [\"1.1.\", #{categories_elem[ 1].id}],
                 [\"1.2.\", #{categories_elem[ 2].id}],
                 [\"1.3.\", #{categories_elem[ 3].id}],
                 [\"1.4.\", #{categories_elem[ 4].id}],
                 [\"1.5.\", #{categories_elem[ 5].id}],
                 [\"2.\",   #{categories_elem[ 6].id}],
                 [\"2.1.\", #{categories_elem[ 7].id}],
                 [\"2.2.\", #{categories_elem[ 8].id}],
                 [\"2.3.\", #{categories_elem[ 9].id}],
                 [\"2.4.\", #{categories_elem[10].id}]]".gsub(/\s+/, "")
  )
end

# Datas

Value.delete_all

Edition.all.each do |e|
  Country.all.each do |c|
    Value.create(edition_id: e.id, country_id: c.id,
      total: rand(0..9) + (rand(0..9) / 10.0),
      values: '[' + (e.categories.collect { |ct| "[#{ct.id}, \"#{rand(0..9)}\"]" } * ',') + ']'
    )
  end
end
